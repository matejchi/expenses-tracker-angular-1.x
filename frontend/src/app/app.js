var app = angular.module("ExpenseTrackerApp", ["templates-app", "templates-common", "ui.router", "ui.bootstrap", "customModules"]);

app.constant("ENV_CONFIG", {
	"DEV_BASE_URL" : "http://localhost:8080/",
	"PROD_BASE_URL" : "http://expense-tracker-app.herokuapp.com/",
	"IS_PROD" : false
});

app.config(["$stateProvider", "$urlRouterProvider", function myAppConfig ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when("main", "index.html");
	$urlRouterProvider.otherwise("/");
}]);

app.run(function run() {
	console.log("Starting app");
});

app.controller("ApplicationController", ["$scope", "$state", function ApplicationController($scope, $state) {

	$scope.flashError = false;

	console.log("ApplicationController");

	$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
		$scope.pageTitle = (toState.data && toState.data.pageTitle) ? ("Expense Tracker App " + toState.data.pageTitle) : "Expense Tracker App";
		$scope.loggedIn = sessionStorage.getItem("loggedIn");

		if (toState.name == "home"){
			if (!$scope.loggedIn){
				$state.go("forbidden");
			}
		}
	});

	$scope.$on("flashMessage", function(event, params){
		$scope.showFlashMessage = true;
		$scope.flashMessage = params.message;
		$scope.flashError = params.error;
	});

	$scope.closeFlashMessages = function(){
		$scope.showFlashMessage = false;
		$scope.flashMessage = "";
	};

	$scope.loggedIn = sessionStorage.getItem("loggedIn");
	$scope.current_user = JSON.parse(sessionStorage.getItem("current_user"));

	if ($scope.loggedIn) {
		switch ($scope.current_user.role) {
			case "admin":
			$state.go("adminpage");
			break;
			case "regular":
			$state.go("userpage");
			break;
			default:
			$state.go("guest");
		}
	} else {
		$state.go("guest");
	}
}]);