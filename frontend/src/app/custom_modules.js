//this file is used to inject all custom modules..
//then this file is injected in the main module Angular app

(function(){

	angular.module("customModules",
	               [
		               "about",
		               "contact",
		               "errors",
		               "guest",
		               "login",
		               "signup",
		               "signup_directives",
		               "admin",
		               "user",
		               "manageUsers",
		               "reports",
		               "shared",
		               "expenses",
		               "new_user_directives"
	               ]);

})();
