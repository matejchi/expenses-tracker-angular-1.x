
app.factory("LoginService", ["$http", "$q", "SharedService", function($http, $q, SharedService){

	var factory = {};
	const BASE_URL = SharedService.getBaseURL();

	factory.login = function(user){

		var defer = $q.defer();

		$http({
			method: "POST",
			url: BASE_URL + "api/sessions",
			data: user,
			headers: { "Content-Type" : "application/x-www-form-urlencoded" },
			transformRequest: function(user){
				var str = [];
				for (var attr in user){
					str.push(encodeURIComponent(attr) + "=" + encodeURIComponent(user[attr]));
				}
				return str.join("&");
			}

		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	},

	factory.logout = function(user){

		var defer = $q.defer();

		$http({
			method: "DELETE",
			url: BASE_URL + "api/sessions/" + user.id,
			headers : { "Content-Type" : "application/x-www-form-urlencoded", "email" : user.email, "api-token" : user.api_token }
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	}

	return factory;
}]);