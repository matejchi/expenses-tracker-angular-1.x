
//best practice: wrap all modules inside IIFE -> IIFE removes variables from the global scope!
(function() {

	//best practice: avoid using a variable and instead use chaining with the getter syntax.
	//no need to introduce another var inside modules:
	/* e.g.: don't do this:         var loginModule = angular.module("login", []);
	                                loginModule.config([$stateProvider", function loginConfig($stateProvider){...}]); */

	angular.module("login", ["ui.router"]);
	angular.module("login").config(["$stateProvider", function loginConfig($stateProvider){

		$stateProvider.state("login", {
			url: "/login",
			controller: "LoginController",
			templateUrl: "login/login.tpl.html",
			data: { pageTitle: " - login"}
		});

		$stateProvider.state("main", {
			url: "/",
			controller: "ApplicationController"
		});
	}]);

	angular.module("login").controller("LoginController", ["$scope", "$state", "LoginService", LoginController]);

	function LoginController($scope, $state, LoginService){

		$scope.login = function(user){

			if (!$scope.loginForm.$valid){
				$scope.$emit("flashMessage", params = {message : "Check your credentials.", data : "", error: true});
				return;
			}

			LoginService.login(user).then(function(loginData){
				$scope.$emit("flashMessage", params = { message : "You've successfully logged in.", data : ""} );
				sessionStorage.setItem("loggedIn", true);
				sessionStorage.setItem("current_user", JSON.stringify(loginData.user));
				$state.go("main");
			}).catch(function(errorRes){
				$scope.$emit("flashMessage", params = { message: errorRes.message, data: "", error: true});
				$scope.user.email = '';
				$scope.user.password = '';
			});
		}

	}

})();