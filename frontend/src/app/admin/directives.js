(function(){

	angular.module("new_user_directives", []);
	angular.module("new_user_directives").directive("passwordMatchNewUser", function passwordMatchNewUser(){

		return {
			restrict: "A",
			require: "ngModel",
			link: function(scope, elem, attrs, ngModel) {
				scope.$watch(attrs.ngModel, function(){
					if (scope.newUserForm.password_confirmation.$viewValue === scope.newUserForm.password.$viewValue){
						scope.newUserForm.password_confirmation.$setValidity("passwordMatchNewUser", true);
					}else {
						scope.newUserForm.password_confirmation.$setValidity("passwordMatchNewUser", false);
					}
				});
			}
		};

	});

})();