(function (){

	angular.module("manageUsers", ["ui.router", "ui.bootstrap"]);
	angular.module("manageUsers").config(["$stateProvider", function manageUsersConfig($stateProvider){}]);

	angular.module("manageUsers").controller("ManageUsersController", ["$scope", "$modalInstance", "UserService", ManageUsersController]);

	function ManageUsersController($scope, $modalInstance, UserService){

		$scope.current_user = JSON.parse(sessionStorage.getItem("current_user"));
		$scope.user = $modalInstance.userDetails;

		$scope.saveUser = function(user){
		   //refactor client-side validation!!
		   UserService.addUser(user, $scope.current_user).then(function(userData){

		   	//here we could simply use "$state.go("main");" and it would refresh our admin page, but, it is "cheaper" to make another call to getUsers() and refresh just one view,
		   	//then to call "main" state and refresh the whole app...
		   	UserService.getUsers($scope.current_user).then(function(usersData){
		   		$scope.users.length = 0;
	            $scope.users.push.apply($scope.users,usersData.users);
		   		$scope.$emit("flashMessage", params = { message: "New user added.", data: ""});
		   		$modalInstance.dismiss();
		   	}).catch(function(errorRes){
		   		$scope.$emit("flashMessage", params = { message: errorRes.message, data: "", error: true});
		   	});
		   }).catch(function(errorRes){
		   	$scope.$emit("flashMessage", params = { message: errorRes.message, data: "", error: true});
		   	alert("Error: " + errorRes.message);
		   });
		}

		$scope.cancel = function(){
			$modalInstance.dismiss();
		};

	}

})();