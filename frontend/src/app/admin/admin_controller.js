var admin_module = angular.module("admin", ["ui.router", "ui.bootstrap"]);

admin_module.config(["$stateProvider", function adminConfig($stateProvider) {
	$stateProvider.state("adminpage", {
		url: "/adminpage",
		controller: "AdminController",
		templateUrl: "admin/admin_page.tpl.html",
		data: { pageTitle: " - Admin Panel"}
	});
}]);

admin_module.controller("AdminController", ["$scope", "$modal", "$window", "ExpenseService", "UserService", function AdminController($scope, $modal, $window, ExpenseService, UserService) {

   $scope.current_user = JSON.parse(sessionStorage.getItem("current_user"));
   $scope.adminRole = $scope.current_user.role == 'admin';

   $scope.expenseFilters = [
      {id: 1, name :"All"},
      {id: 2, name : "Last Week"},
      {id: 3, name : "Last Month"},
      {id: 4, name : "Amount < 100"},
      {id: 5, name : "Amount >= 100"},
      {id: 6, name : "Today"}
   ];

   $scope.selectedFilter = $scope.expenseFilters[0];

   ExpenseService.getAllExpenses($scope.current_user).then(function(expensesData){
      $scope.adminExpenses = expensesData.expenses;
   }).catch(function(errorRes){
      $scope.$emit("flashMessage", params = { message: errorRes.message, data: "", error: true});
   });

   UserService.getUsers($scope.current_user).then(function(usersData){
      $scope.users = usersData.users;
   }).catch(function(errorRes){
      $scope.$emit("flashMessage", params = { message: errorRes.message, data: "", error: true});
   });

   $scope.newExpense = function(){
      var modalInstance = $modal.open({
         templateUrl: 'expenses/new_expense.tpl.html',
         controller: "ExpenseController",
         scope: $scope //pass the scope to child controller (to AdminExpenseController => modal controller)
      });
   }

   $scope.newUser = function(){
      var modalInstance = $modal.open({
         templateUrl: 'user/new_user.tpl.html',
         controller: 'ManageUsersController',
         scope: $scope
      });
   }

   $scope.showExpense = function(expense){
      var modalInstance = $modal.open({
         templateUrl: 'expenses/expense_details.tpl.html',
         controller: "ExpenseController",
      });

      modalInstance.expenseDetails = expense;
   }

   $scope.deleteExpense = function(expense){
      ExpenseService.deleteExpense(expense, $scope.current_user).then(function(deleteExData){
         ExpenseService.getAllExpenses($scope.current_user).then(function(expensesData){
            $scope.adminExpenses = expensesData.expenses;
            $scope.$emit("flashMessage", params = { message: "Expense deleted.", data: ""});
         }).catch(function(errorData){
            $scope.$emit("flashMessage", params = { message: errorData.message, data: "", error: true});
         });
      }).catch(function(errorDeleteData){
         $scope.$emit("flashMessage", params = { message: errorDeleteData.message, data: "", error: true});
      });
   }

   $scope.editExpense = function(expense){

      //todo -> implement client-side validation
      var modalInstance = $modal.open({
         templateUrl: 'expenses/edit_expense.tpl.html',
         controller: "ExpenseController",
         scope: $scope
      });

      /* this is necessery, because if we pass the same object (ref) to ExpenseController, when we open a modal and start changing the
         "expense" object, it will automatically reflect changes to scope beneath, therfore object that is bind on this scope, will be changed
         no matter if we save the actual changes on server, or not! */
      var anotherExpense = {};
      angular.copy(expense, anotherExpense);
      modalInstance.editExpense = anotherExpense;
   }

   $scope.showUser = function(user){
      var modalInstance = $modal.open({
         templateUrl: 'user/user_details.tpl.html',
         controller: 'ManageUsersController'
      });

      modalInstance.userDetails = user;
   }

   $scope.deleteUser = function(user){

      if ($window.confirm("Are you sure you want to delete user's account?")) {
         UserService.deleteUser(user, $scope.current_user).then(function(deleteUserData){
            UserService.getUsers($scope.current_user).then(function(usersData){
               $scope.users = usersData.users;
               $scope.$emit("flashMessage", params = { message: "User deleted.", data: ""});
            }).catch(function(errorUsersData){
               $scope.$emit("flashMessage", params = { message: errorUsersData.message, data: "", error: true});
            });
         }).catch(function(errorDeleteData){
            $scope.$emit("flashMessage", params = { message: errorDeleteData.message, data: "", error: true});
         });
      }
   }

   $scope.addUser = function(){
      alert("not implemented yet...");
   }

   $scope.editUser = function(user){
      alert("not implemented yet...");
   }

   $scope.applyFilter = function(filter){
      ExpenseService.filterBy($scope.current_user, filter).then(function(filteredData){
         $scope.adminExpenses = filteredData.expenses;
      }).catch(function(errorRes){
         $scope.$emit("flashMessage", params = { message: errorRes.message, data: "", error: true});
      });
   }

   $scope.openReportModal = function(){
      var modalInstance = $modal.open({
         templateUrl: 'reports/report.tpl.html',
         controller: 'ReportsController'
      });
   }

}]);