
app.factory("SharedService", ["ENV_CONFIG", function(ENV_CONFIG){

	var factory = {};

	factory.getBaseURL = function(){
		const BASE_URL = ENV_CONFIG.IS_PROD ? ENV_CONFIG.PROD_BASE_URL : ENV_CONFIG.DEV_BASE_URL;
		return BASE_URL;
	}

	return factory;
}]);