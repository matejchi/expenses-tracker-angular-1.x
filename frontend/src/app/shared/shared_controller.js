
(function(){

	angular.module("shared", ["ui.router"]);
	angular.module("shared").config(["$stateProvider", function sharedConfig($stateProvider){}]);

	angular.module("shared").controller("SharedController", ["$scope", SharedController]);

	function SharedController($scope){
		$scope.current_user = JSON.parse(sessionStorage.getItem("current_user"));
	}

})();