
(function(){

	angular.module("guest", ["ui.router"]);
	angular.module("guest").config(["$stateProvider", function guestConfig($stateProvider){
		$stateProvider.state("guest", {
			url: "/guest",
			controller: "GuestController",
			templateUrl: "guest/guest.tpl.html"
		});
	}]);

	angular.module("guest").controller("GuestController", ["$scope", GuestController]);

	function GuestController($scope){

	}

})();