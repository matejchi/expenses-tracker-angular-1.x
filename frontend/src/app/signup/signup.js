(function() {

	angular.module("signup", ["ui.router"]);
	angular.module("signup").config(["$stateProvider", function signupConfig($stateProvider){
		$stateProvider.state("signup", {
			url: "/signup",
			controller: "SignupController",
			templateUrl: "signup/signup.tpl.html",
			data: { pageTitle: " - Signup"}
		});
	}]);

	angular.module("signup").controller("SignupController", ["$scope", "$state", "SignupService", "LoginService", SignupController]);

	function SignupController($scope, $state, SignupService, LoginService){

		$scope.signup = function(newUser){
			if ($scope.signupForm.$invalid || newUser === undefined){
				$scope.$emit("flashMessage", params = { message : "Check your signup data.", data : "", error: true});
				return false;
			} else {
				SignupService.register(newUser).then(function(registerData){
					$scope.$emit("flashMessage", params = { message : "You've successfully created your account. Please log in.", data : ""});
					$state.go("login");
				}).catch(function(errorRes){
					$scope.$emit("flashMessage", params = {message : errorRes.message, data : "", error: true});
				});
			}
		};

		$scope.logout = function(){
			$scope.current_user = JSON.parse(sessionStorage.getItem("current_user"));

			LoginService.logout($scope.current_user).then(function(logoutData){
				sessionStorage.removeItem("loggedIn");
				sessionStorage.removeItem("current_user");
				$scope.$emit("flashMessage", params = { message : "You've been logged out.", data : ""});
				$state.go("guest");
			}).catch(function(errorRes){
				$scope.$emit("flashMessage", params = {message : errorRes.message, data : "", error: true});
			});
		};
	}

})();