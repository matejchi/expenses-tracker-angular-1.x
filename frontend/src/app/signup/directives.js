
(function(){

	angular.module("signup_directives", []);
	angular.module("signup_directives").directive("passwordMatch", function passwordMatch(){

		return {
			restrict: "A",
			require: "ngModel",
			link: function(scope, elem, attrs, ngModel) {
				scope.$watch(attrs.ngModel, function(){
					if (scope.signupForm.password_confirmation.$viewValue === scope.signupForm.password.$viewValue){
						scope.signupForm.password_confirmation.$setValidity("passwordMatch", true);
					}else {
						scope.signupForm.password_confirmation.$setValidity("passwordMatch", false);
					}
				});
			}
		};

	});

})();