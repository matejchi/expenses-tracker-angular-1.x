
app.factory("SignupService", ["$http", "$q", "SharedService", function($http, $q, SharedService){

	var factory = {};
	const BASE_URL = SharedService.getBaseURL();

	factory.register = function(newUser){

		var defer = $q.defer();

		$http({
			method: "POST",
			url: BASE_URL + "api/users",
			data: newUser,
			headers : { "Content-Type" : "application/x-www-form-urlencoded" },
			transformRequest: function(newUser){
				var str = [];
				for (var attr in newUser){
					str.push(encodeURIComponent(attr) + "=" + encodeURIComponent(newUser[attr]));
				}
				return str.join("&");
			}

		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	}

	return factory;
}]);