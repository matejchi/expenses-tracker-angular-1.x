
app.factory("ExpenseService", ["$http", "$q", "SharedService", function($http, $q, SharedService){

	var factory = {};
	const BASE_URL = SharedService.getBaseURL();

	factory.saveExpense = function(newExpense, user){

		var defer = $q.defer();

		$http({
			method: "POST",
			url: BASE_URL + "api/users/" + user.id + "/expenses",
			data: newExpense,
			headers : { "Content-Type" : "application/x-www-form-urlencoded", "email" : user.email, "api-token" : user.api_token },
			transformRequest: function(newExpense){
				var str = [];
				for (var attr in newExpense){
					str.push(encodeURIComponent(attr) + "=" + encodeURIComponent(newExpense[attr]));
				}
				return str.join("&");
			}
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	},

	factory.getAllExpenses = function(user){

		var defer = $q.defer();

		$http({
			method: "GET",
			url: BASE_URL + "api/users/" + user.id + "/expenses",
			headers : { "email" : user.email, "api-token" : user.api_token }
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	},

	factory.deleteExpense = function(expense, user){

		var defer = $q.defer();

		$http({
			method: "DELETE",
			url: BASE_URL + "api/users/" + user.id + "/expenses/" + expense.id,
			headers : { "email" : user.email, "api-token" : user.api_token }
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	},

	factory.filterBy = function(user, filter){

		var defer = $q.defer();

		$http({
			method: "GET",
			url: BASE_URL + "api/users/" + user.id + "/expenses/filter?type=" + filter.id,
			headers : {"email" : user.email, "api-token" : user.api_token }
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;

	},

	factory.editExpense = function(expense, user){

		var defer = $q.defer();

		$http({
			method: "PUT",
			url: BASE_URL + "api/users/" + user.id + "/expenses/" + expense.id,
			data: expense,
			headers : { "Content-Type" : "application/x-www-form-urlencoded", "email" : user.email, "api-token" : user.api_token },
			transformRequest: function(expense){
				var str = [];
				for (var attr in expense){
					str.push(encodeURIComponent(attr) + "=" + encodeURIComponent(expense[attr]));
				}
				return str.join("&");
			}
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	}

	return factory;
}]);