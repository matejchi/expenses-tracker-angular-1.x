(function(){

	angular.module("expenses", ["ui.bootstrap"]);
	angular.module("expenses").config(["$stateProvider", function expensesConfig($stateProvider) {}]);

	angular.module("expenses").controller("ExpenseController", ["$scope", "$modalInstance", "ExpenseService", ExpenseController]);

	function ExpenseController($scope, $modalInstance, ExpenseService) {

		$scope.current_user = JSON.parse(sessionStorage.getItem("current_user"));
		$scope.adminRole = $scope.current_user.role == 'admin';

		$scope.expenseDetails = $modalInstance.expenseDetails;

		//parse time attribute to "fit" timepicker
		if ($modalInstance.editExpense) {
			$scope.editExpense = $modalInstance.editExpense;

			if (typeof $modalInstance.editExpense.time === "string"){
				var time = $modalInstance.editExpense.time;
				var arr = time.split(":");
				var x = new Date();
				x.setHours(arr[0]);
				x.setMinutes(arr[1]);
				$scope.editExpense.time = x;
			}
		}

		$scope.save = function(expense){
   		//todo -> implement client-side validation!!!
   		if (expense === undefined) return;

	   	var newExpense = {};
	   	newExpense.description = expense.desc;
	   	newExpense.amount = expense.amount;
	   	newExpense.date = expense.date;
	   	newExpense.time = expense.time;
	   	newExpense.comment = expense.comment;

	   	ExpenseService.saveExpense(newExpense, $scope.current_user).then(function(saveExpenseData){
	   		ExpenseService.getAllExpenses($scope.current_user).then(function(expensesData){
	   			$scope.adminExpenses.length = 0;
	            $scope.adminExpenses.push.apply($scope.adminExpenses,expensesData.expenses); //access the parent controller => AdminController

	            /* Emiting from modal using "$scope.$emit()" is possible because we passed parent's scope (scope managed by the AdminController) to this one,
	               when we instantiated a modal object in "admin_controller.js". In case we didn't passed the parent's scope, we would then need to use "$rootScope.$broadcast()",
	               to broadcast the "flashMessage" e.g.: $rootScope.$broadcast("flashMessage", params = { message: "New expense created!", data : ""}); */
	            $scope.$emit("flashMessage", params = { message: "New expense created!", data : ""});
	            $modalInstance.dismiss();

	         }).catch(function(errorData){
	            $scope.$emit("flashMessage", params = { message: errorData.message, data: "", error: true});
	            $modalInstance.dismiss();
	         });
	      }).catch(function(errorRes){
	         $scope.$emit("flashMessage", params = { message: errorRes.message, data: "", error: true});
	         alert("Error: " + errorRes.message );
	         // $modalInstance.dismiss();
	      });
      };

      $scope.update = function(expense){
      	//todo -> implement client-side validation!!!
      	ExpenseService.editExpense(expense, $scope.current_user).then(function(editExData){
      		ExpenseService.getAllExpenses($scope.current_user).then(function(expensesData){
      			$scope.adminExpenses.length = 0;
	            $scope.adminExpenses.push.apply($scope.adminExpenses,expensesData.expenses);
      			$scope.$emit("flashMessage", params = { message: "Expense successfully changed.", data: ""});
      			$modalInstance.dismiss();
      		}).catch(function(errorData){
      			$scope.$emit("flashMessage", params = { message: errorData.message, data: "", error: true});
      			$modalInstance.dismiss();
      		});
      	}).catch(function(errorEditData){
      		$scope.$emit("flashMessage", params = { message: errorEditData.message, data: "", error: true});
      		alert("Error: " + errorEditData.message );
      	});

      }

      $scope.cancel = function(){
      	$modalInstance.dismiss();
      };
   }

})();