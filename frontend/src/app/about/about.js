
(function(){

	angular.module("about", ["ui.router"]);

	angular.module("about").config(["$stateProvider", function aboutConfig($stateProvider) {
		$stateProvider.state("about", {
			url: "/about",
			controller: "AboutController",
			templateUrl: "about/about.tpl.html",
			data: { pageTitle: " - About"}
		});
	}]);

	angular.module("about").controller("AboutController", ["$scope", AboutController]);

	function AboutController($scope) {

	}

})();