
(function(){

	angular.module("reports", ["ui.router", "ui.bootstrap"]);
	angular.module("reports").config(["$stateProvider", function manageUsersConfig($stateProvider){}]);

	angular.module("reports").controller("ReportsController", ["$scope", "$modalInstance", "$window", "ReportsService", ReportsController]);

	function ReportsController($scope, $modalInstance, $window, ReportsService){

		var fileTypes = {
			pdf: "application/pdf",
			csv: "application/csv",
			plainText: "text/plain",
			msword: "application/msword",
			excel: "application/excel"
		}

		console.log("ReportsController");

		$scope.current_user = JSON.parse(sessionStorage.getItem("current_user"));
		$scope.adminRole = $scope.current_user.role == 'admin';

		$scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		$scope.format = $scope.formats[0];

		$scope.startDate = new Date();
		$scope.endDate = new Date();
		$scope.sd = { opened: false };
		$scope.ed = { opened: false };

		$scope.openStartDate = function() {
			$scope.sd.opened = true;
		};

		$scope.openEndDate = function() {
			$scope.ed.opened = true;
		};

		$scope.generateReport = function(startDate, endDate){
		//for some reason angular-ui-bootstrap's "datepicker-popup" directive doesn't bind with this $scope/model, so I'm passing date params manually

			var period = {report_date_from: startDate, report_date_to: endDate };

			ReportsService.downloadReport(period, $scope.current_user).then(function(reportData){
				var file = new Blob([reportData], {type: fileTypes.pdf });
      		var fileURL = URL.createObjectURL(file);
				$window.open(fileURL);
				$modalInstance.dismiss();
			}).catch(function(errorRes){
				$scope.$emit("flashMessage", params = { message: errorRes.message, data: "", error: true});
			});
		};

		$scope.cancel = function(){
			$modalInstance.dismiss();
		};

	}

})();