
app.factory("ReportsService", ["$http", "$q", "SharedService", function($http, $q, SharedService){

	var factory = {};
	const BASE_URL = SharedService.getBaseURL();

	factory.downloadReport = function(period, user){

		var defer = $q.defer();

		var startDate = period.report_date_from.toISOString().slice(0,10);
		var endDate = period.report_date_to.toISOString().slice(0,10);

		$http({
			method: "GET",
			url: BASE_URL + "api/users/" + user.id + "/report?report_date_from=" + startDate + "&report_date_to=" + endDate,
			headers : { "email" : user.email, "api-token" : user.api_token, "Accept": "application/pdf" },
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	}

	return factory;
}]);