
app.factory("UserService", ["$http", "$q", "SharedService", function($http, $q, SharedService){

	var factory = {};
	const BASE_URL = SharedService.getBaseURL();

	factory.getUsers = function(user){

		var defer = $q.defer();

		$http({
			method: "GET",
			url: BASE_URL + "api/users/",
			headers : { "email" : user.email, "api-token" : user.api_token }
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	},


	factory.deleteUser = function(userToDelete, user){

		var defer = $q.defer();

		$http({
			method: "DELETE",
			url: BASE_URL + "api/users/" + userToDelete.id,
			headers : { "email" : user.email, "api-token" : user.api_token }
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	},

	factory.addUser = function(userToAdd, user){

		var defer = $q.defer();

		$http({
			method: "POST",
			url: BASE_URL + "api/users",
			data: userToAdd,
			headers : { "Content-Type" : "application/x-www-form-urlencoded", "email" : user.email, "api-token" : user.api_token },
			transformRequest: function(userToAdd){
				var str = [];
				for (var attr in userToAdd){
					str.push(encodeURIComponent(attr) + "=" + encodeURIComponent(userToAdd[attr]));
				}
				return str.join("&");
			}
		}).success(function(data, status, headers, config){
			defer.resolve(data);
		}).error(function(data, status, headers, config){
			defer.reject(data);
		});

		return defer.promise;
	}

	return factory;
}]);