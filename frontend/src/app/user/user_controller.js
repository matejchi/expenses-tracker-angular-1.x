
(function (){

	angular.module("user", ["ui.router", "ui.bootstrap"]);
	angular.module("user").config(["$stateProvider", function loginConfig($stateProvider){
		$stateProvider.state("userpage", {
			url: "/userpage",
			controller: "UserController",
			templateUrl: "user/user_page.tpl.html",
			data: { pageTitle: " - User Panel"}
		});
	}]);

	angular.module("user").controller("UserController", ["$scope", "$modal", "ExpenseService", UserController]);

	function UserController($scope, $modal, ExpenseService){
		$scope.current_user = JSON.parse(sessionStorage.getItem("current_user"));
		$scope.adminRole = $scope.current_user.role == 'admin';
	}

})();