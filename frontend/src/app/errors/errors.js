
(function(){

	angular.module("errors", []);
	angular.module("errors").config(["$stateProvider", function errorsConfig($stateProvider){
		$stateProvider.state("forbidden", {
			url: "/forbidden",
			templateUrl: "errors/forbidden.tpl.html",
			controller: "ErrorsController"
		});
	}]);

	angular.module("errors").controller("ErrorsController", ["$scope", ErrorsController]);

	function ErrorsController($scope){
	}

})();