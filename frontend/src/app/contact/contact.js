
(function(){

	angular.module("contact", ["ui.router"]);
	angular.module("contact").config(["$stateProvider", function contactConfig($stateProvider){
		$stateProvider.state("contact", {
			url: "/contact",
			controller: "ContactController",
			templateUrl: "contact/contact.tpl.html",
			data: {pageTitle: " - Contact"}
		});
	}]);

	angular.module("contact").controller("ContactController", ["$scope", ContactController]);

	function ContactController($scope){

	}

})();