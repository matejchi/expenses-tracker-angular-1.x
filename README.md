### This app is written using Ruby on Rails as a back-end REST API, and Angular v1.2.9 as front-end. It's purpose is just to show Angular coding style and practices... Angular code can be found in "/frontend/src" folder. ###

Setting-up the app:
-----------------------------
Dependencies:
	Ruby ver. 2.1.4
	PostgreSQL
	NodeJS

1. Install RVM, Ruby (2.1.4), Bundler and PostgreSQL servers
2. Run `bundle install`
3. Run `rake db:migrate`, to create DB and schema
4. Run `rake db:seed`, to seed data
5. Run `rake users:admin_init`, to initialize admin account
6. Boot the app by typing `unicorn` (or if you prefer rails' built-in WebBrick server, then run the app by typing `rails s`)
7. Open you browser and type `http://localhost:8080`, if you're using Unicorn web server, or `http://localhost:3000` if you prefer WebBrick
8. To login to app use credentials:
	`email: admin@isp.net`
	`pass: root`

FrontEnd:
---------
1. `sudo npm -g install grunt-cli karma bower`
2. `npm install`
3. `bower install`
4. `grunt watch`
5. to compile "frontend app", run: `grunt -f`